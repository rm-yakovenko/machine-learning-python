import matplotlib.pyplot as plt
import scipy as sp
from sklearn.cross_validation import train_test_split


def error(f, x, y):
    return sp.sum((f(x) - y) ** 2)


def plot(f):
    fx = sp.linspace(0, x[-1], 1000)
    legend = "d=%i" % f.order
    plt.plot(fx, f(fx), linewidth=2, label=legend)


data = sp.genfromtxt("web_traffic.csv", delimiter=",")

x = data[:, 0]
y = data[:, 1]

x = x[~sp.isnan(y)]
y = y[~sp.isnan(y)]

x_train, x_check, y_train, y_check = train_test_split(
    x, y, test_size=0.2, random_state=0)

plt.scatter(x, y, s=10)
plt.title("Web traffic over the last month")
plt.xlabel("Time")
plt.ylabel('Hits/hour')
plt.autoscale(tight=True)
plt.grid(True, linestyle='-', color='0.75')

print "| Degree | Error |"
print "------------------"
for degree in range(1, 11):
    fp = sp.polyfit(x, y, degree)
    f = sp.poly1d(fp)
    plot(f)
    print "| %d | %.2f |" % (degree, error(f, x_check, y_check))
print "------------------"

plt.show()
